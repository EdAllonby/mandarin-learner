﻿namespace MandarinLearner.Model
{
    public class PhraseMapper
    {
        public string English { get; set; }

        public string Pinyin { get; set; }

        public string Chinese { get; set; }
    }
}